'use strict';

const jetpack = require('fs-jetpack');
const Promise = require('bluebird');
const uuid = require('uuid');
const path = require('path');

const idPattern = /^[a-z0-9_-]+$/i;

class FSDoc {

    /**
     * Creates a new FSDoc instance.
     *
     * @param {Object} opts
     * @param {jetpack} opts.dir
     * @param {String} opts.path
     */
    constructor(opts) {
        opts = opts || {};

        if (opts.dir) {
            this._cwd = opts.dir;
        } else if (opts.path) {
            this._cwd = jetpack.dir(opts.path);
        } else {
            throw new Error('Unspecified dir or path');
        }
    }

    /**
     * Returns if the given ID is valid. IDs must be at least one character in
     * length and consist of letters, numbers, underscores, and dashes.
     *
     * @param id
     * @return {boolean}
     */
    static valid(id) {
        return idPattern.test(id);
    }

    /**
     * Returns if a doc with the given ID exists.
     *
     * @param {String} id
     * @return {Promise.boolean}
     */
    exists(id) {
        if (!FSDoc.valid(id)) {
            return Promise.reject(new Error('invalid ID'));
        }

        return Promise.resolve(this._cwd.existsAsync(id + '.json'))
            .then((value) => {
                return value === 'file';
            });
    }

    /**
     * Removes the doc with the given ID. Returns the ID.
     *
     * @param {String} id
     * @return {Promise.String}
     */
    remove(id) {
        if (!FSDoc.valid(id)) {
            return Promise.reject(new Error('invalid ID'));
        }

        return Promise.resolve(this._cwd.removeAsync(id + '.json'))
            .then(() => {
                return id;
            });
    }

    /**
     * Gets a document by ID.
     *
     * @param {String} id
     * @return {Promise.Object}
     */
    get(id) {
        if (!FSDoc.valid(id)) {
            return Promise.reject(new Error('invalid ID'));
        }

        return Promise.resolve(this._cwd.readAsync(id + '.json', 'utf8'))
            .then((data) => {
                if (data === null) {
                    return Promise.reject('invalid id ' + id);
                }
                try {
                    var doc = JSON.parse(data);
                    if (doc === null || typeof doc !== 'object' || !doc.hasOwnProperty('id')) {
                        return Promise.reject(id + ' is not a valid doc');
                    }
                    if (doc.id !== id) {
                        return Promise.reject('mismatched document id');
                    }
                    return doc;
                } catch (ex) {
                    return Promise.reject(ex);
                }
            });
    }


    /**
     * Sets a document. doc must contain an 'id' property, or else a random ID
     * will be generated. doc is returned.
     *
     * @param {Object} doc
     * @return {Promise.Object}
     */
    set(doc) {
        if (doc === null) {
            return Promise.reject('null doc');
        }
        if (typeof doc !== 'object') {
            return Promise.reject('doc is not an object');
        }
        if (!doc.hasOwnProperty('id')) {
            doc.id = uuid.v4();
        } else {
            if (!FSDoc.valid(doc.id)) {
                return Promise.reject(new Error('invalid ID'));
            }
        }
        const json = JSON.stringify(doc);

        return Promise.resolve(this._cwd.writeAsync(doc.id + '.json', json, {atomic: true}))
            .then(() => {
                return doc;
            });
    }

    /**
     * Returns an array of all document IDs.
     *
     * @return {Promise.Array.String}
     */
    list() {
        return Promise.resolve(this._cwd.listAsync('.'))
            .then((filenames) => {
                var ids = [];
                for (var i = 0; i < filenames.length; i++) {
                    const filename = filenames[i];
                    if (!filename.endsWith('.json')) {
                        continue;
                    }
                    const id = path.basename(filename, '.json');
                    if (!FSDoc.valid(id)) {
                        continue;
                    }
                    ids.push(id);
                }
                return ids;
            });
    }

    /**
     * Removes all documents. Returns an array of the IDs that were removed.
     *
     * @return {Promise.Array.String}
     */
    clear() {
        return this.list()
            .each((id) => {
                return this._cwd.removeAsync(id + '.json');
            });
    }

    /**
     * Counts the number of documents.
     *
     * @return {Promise.Number}
     */
    count() {
        return this.list()
            .then((ids) => {
                return ids.length;
            });
    }

    /**
     * Returns all documents.
     *
     * @return {Promise.Array.Object}
     */
    all() {
        return this.list()
            .map((id) => {
                return this.get(id);
            });
    }
}

module.exports = FSDoc;
