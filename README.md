# FSDoc

simple, promise-based, file system document store

## Install

    npm install --save bitbucket:labnet/fsdoc#v0.3.0

## Usage

    const FSDoc = require('fsdoc');

    const fsdoc = new FSDoc({
        // Use path OR dir option
        path: 'users',
        dir: require('jetpack').dir('users'),
    });


    /**
     * Returns if a doc with the given ID exists.
     *
     * @param {String} id
     * @return {Promise.boolean}
     */
    fsdoc.exists(id)


    /**
     * Removes the doc with the given ID. Returns the ID.
     *
     * @param {String} id
     * @return {Promise.String}
     */
    fsdoc.remove(id)


    /**
     * Gets a document by ID.
     *
     * @param {String} id
     * @return {Promise.Object}
     */
    fsdoc.get(id)

    /**
     * Sets a document. doc must contain an 'id' property, or else a random ID
     * will be generated. doc is returned.
     *
     * @param {Object} doc
     * @return {Promise.Object}
     */
    fsdoc.set(doc)


    /**
     * Returns an array of all document IDs.
     *
     * @return {Promise.Array.String}
     */
    fsdoc.list()


    /**
     * Removes all documents. Returns an array of the IDs that were removed.
     *
     * @return {Promise.Array.String}
     */
    fsdoc.clear()


    /**
     * Counts the number of documents.
     *
     * @return {Promise.Number}
     */
    fsdoc.count()


    /**
     * Returns all documents.
     *
     * @return {Promise.Array.Object}
     */
    fsdoc.all()
