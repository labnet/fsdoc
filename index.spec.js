'use strict';

const expect = require('expect.js');
const jetpack = require('fs-jetpack');
const os = require('os');

const FSDoc = require('./index.js');

describe('FSDoc', function () {

    var db = new FSDoc({
        dir: jetpack.dir(os.tmpdir()).dir('FSDoc-spec', {empty: true})
    });

    it('empty initial directory', function () {
        return db.count()
            .then((count) => {
                expect(count).to.be(0);
                return db.all();
            })
            .then((docs) => {
                expect(docs).to.be.empty();
            })
            .catch((err) => {
                expect().fail(err);
            });
    });

    it('get non-existant doc', function () {
        return db.get('bad')
            .then(() => {
                expect().fail('should not have reached here');
            })
            .catch((err) => {
                expect(err).not.to.be(null);
            });
    });

    it('remove non-existant doc', function () {
        return db.remove('bad')
            .then((id) => {
                expect(id).to.be('bad');
            })
            .catch((err) => {
                expect().fail('should not have reached here');
            });
    });

    var docIDs = [];

    it('set doc with ID', function () {
        var doc = {
            id: 'test',
            value: 12345
        };
        return db.set(doc)
            .then((savedDoc) => {
                docIDs.push(savedDoc.id);
                docIDs.sort();
                expect(savedDoc).to.eql(doc);
            })
            .catch((err) => {
                expect().fail('should not have reached here');
            });
    });

    it('set doc without ID', function () {
        var doc = {
            value: 12345
        };
        return db.set(doc)
            .then((savedDoc) => {
                docIDs.push(savedDoc.id);
                docIDs.sort();
                expect(savedDoc.id).not.to.be(null);
            })
            .catch((err) => {
                expect().fail('should not have reached here');
            });
    });

    it('two documents', function () {
        return db.count()
            .then((count) => {
                expect(count).to.be(2);
            })
            .catch((err) => {
                expect().fail(err);
            });
    });

    it('documents ids', function () {
        return db.list()
            .then((ids) => {
                ids.sort();
                expect(ids).to.eql(docIDs);
            })
            .catch((err) => {
                expect().fail(err);
            });
    });


    it('overwrite existing ID', function () {
        var doc = {
            id: 'test',
        };
        return db.set(doc)
            .then((savedDoc) => {
                expect(savedDoc.id).to.be(doc.id);
                return db.count();
            })
            .then((count) => {
                expect(count).to.be(2);
            })
            .catch((err) => {
                expect().fail('should not have reached here');
            });
    });

    it('get all documents', function () {
        return db.all()
            .then((docs) => {
                expect(docs.length).to.be(2);

                var ids = [];
                docs.forEach((doc) => {
                    ids.push(doc.id);
                });
                ids.sort();

                expect(ids).to.eql(docIDs);
            })
            .catch((err) => {
                expect().fail(err);
            });
    });

    it('remove single doc', function () {
        return db.remove('test')
            .then(() => {
                return db.count();
            })
            .then((count) => {
                expect(count).to.be(1);
            })
            .catch((err) => {
                expect().fail('should not have reached here');
            });
    });

    it('remove doc with invalid ID', function () {
        return db.remove('')
            .then(() => {
                expect().fail('should not reach here');
            })
            .catch((err) => {
                expect(err).not.to.be(null);
            });
    });

    it('remove all docs', function () {
        return db.clear()
            .then(() => {
                return db.count();
            })
            .then((count) => {
                expect(count).to.be(0);
            })
            .catch((err) => {
                expect().fail(err);
            });
    });

    describe('IDs', function () {

        it('valid', function () {
            var arr = [
                'a',
                '_',
                '-',
                '1',
                '1324234234',
                'a2_ffasd--'
            ];

            arr.forEach(function (e) {
                expect(FSDoc.valid(e)).to.be(true);
            });
        });

        it('invalid', function () {
            var arr = [
                '',
                '#nodejs',
                'helloworld!',
                '../../../node',
                '*'
            ];

            arr.forEach(function (e) {
                expect(FSDoc.valid(e)).to.be(false);
            });
        });
    });

});
